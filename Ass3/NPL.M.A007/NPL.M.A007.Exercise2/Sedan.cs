﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Sedan: Car
    {
        public int Length { get; set; }

        public Sedan() { }

        public Sedan(decimal speed, double regularPrice, string color, int length) : base(speed, regularPrice, color)
        {
            this.Length = length;
        }

        public override double GetSalePrice()
        {
            if (Length > 20)
                return this.RegularPrice - this.RegularPrice * 5 / 100;
            else return this.RegularPrice - this.RegularPrice * 10 / 100;
        }
    }
}
