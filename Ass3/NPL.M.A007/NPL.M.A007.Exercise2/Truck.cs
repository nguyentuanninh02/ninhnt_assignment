﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Truck: Car
    {
        public int Weight {  get; set; }
        
        public Truck() { }

        public Truck(decimal speed, double regularPrice, string color, int Weight) : base(speed, regularPrice, color)
        {
            this.Weight= Weight;
        }

        public override double GetSalePrice()
        {
            if (Weight >200) 
                return this.RegularPrice - this.RegularPrice * 10 / 100;
            else return this.RegularPrice - this.RegularPrice * 20 / 100;
        }
    }
}
