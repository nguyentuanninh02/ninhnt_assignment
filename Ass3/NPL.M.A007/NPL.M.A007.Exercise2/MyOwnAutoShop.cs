﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class MyOwnAutoShop
    {
        Sedan sedan = new Sedan(100, 1000, "black", 25);
        Ford ford1 = new Ford(100, 2000, "red", 2020, 100);
        Ford ford2 = new Ford(100, 2000, "red", 2020, 100);
        Truck truck1 = new Truck(100, 4000, "blue", 200);
        Truck truck2 = new Truck(100, 4000, "blue", 200);

        public void DisplayPriceofCars()
        {
            Console.WriteLine(sedan.GetSalePrice());
            Console.WriteLine(ford1.GetSalePrice());
            Console.WriteLine(ford2.GetSalePrice());
            Console.WriteLine(truck1.GetSalePrice());
            Console.WriteLine(truck2.GetSalePrice());
        }
    }
}
