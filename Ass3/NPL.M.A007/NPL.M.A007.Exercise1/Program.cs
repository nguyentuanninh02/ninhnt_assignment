﻿using NPL.M.A007.Exercise1;

Console.OutputEncoding= System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;

Book book = new Book("Harry Potter", 123456789, "J.K.Rowling", "Kim Dong");
Console.WriteLine(string.Format("{0, -20}{1, -20}{2, -20}{3, -20}", "ISBN Number", "Book Name", "Author Name", "Publisher Name"));
Console.WriteLine(book.GetBookInfomation());