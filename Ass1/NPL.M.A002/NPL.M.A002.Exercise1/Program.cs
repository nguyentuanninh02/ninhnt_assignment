﻿Console.WriteLine("Input array length: ");
int a = Convert.ToInt32(Console.ReadLine());
int[] arr = new int[a];

Console.WriteLine("Input value: ");
for (int i = 0; i < a; i++)
{
    arr[i] = Math.Abs(Convert.ToInt32(Console.ReadLine()));
}

int maxValue = int.MinValue;
int minValue = int.MaxValue;

for (int i = 0; i < arr.Length; i++)
{
    if (arr[i] > maxValue) maxValue = arr[i];
    if (arr[i] < minValue) minValue = arr[i];
}

Console.WriteLine("Max: " + maxValue);
Console.WriteLine("Min: " + minValue);

