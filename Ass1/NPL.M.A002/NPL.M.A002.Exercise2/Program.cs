﻿static int GCD(int a, int b)
{
    if (a == 0) return b;
    if (b == 0) return a;

    if (a > b)  return GCD(b, a % b);
    else if (a < b) return GCD(a, b % a);
    else return a;

}

Console.WriteLine("Input a: ");
int a = Math.Abs(Convert.ToInt32(Console.ReadLine()));
Console.WriteLine("Input b: ");
int b = Math.Abs(Convert.ToInt32(Console.ReadLine()));
Console.WriteLine("GCD: " + GCD(a, b));
