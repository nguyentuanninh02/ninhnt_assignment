static int GCD(int a, int b)
{
    if (a == 0) return b;
    if (b == 0) return a;

    if (a > b)  return GCD(b, a % b);
    else if (a < b) return GCD(a, b % a);
    else return a;

}

Console.WriteLine("Input length: ");
int a = Convert.ToInt32(Console.ReadLine());
Console.WriteLine("Input value: ");

int[] arr = new int[a];
for (int i = 0; i < a; i++)
{
    arr[i] = Math.Abs(Convert.ToInt32(Console.ReadLine()));
}

int index = 2;
int tmp = GCD(arr[0], arr[1]);
while (index < arr.Length)
{
    tmp = GCD(tmp, arr[index]);
    index++;
}

Console.WriteLine("Result: "+ tmp);
    

