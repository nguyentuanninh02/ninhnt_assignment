﻿// See https://aka.ms/new-console-template for more information
using System.Text;

Console.OutputEncoding = Encoding.UTF8;
Console.Write("Input a: ");
double a = Convert.ToInt32(Console.ReadLine());
Console.Write("Input b: ");
double b = Convert.ToInt32(Console.ReadLine());
Console.Write("Input c: ");
double c = Convert.ToInt32(Console.ReadLine());

if(a!= 0)
{
    double delta = b * b - 4 * a * c;
    if (delta < 0)
    {
        Console.WriteLine("Vô nghiệm");
    }
    else if (delta == 0)
    {
        Console.WriteLine("Nghiệp kép: x1= x2= " + (-b / 2 / a));
    }
    else
    {
        Console.WriteLine("x1: " + (-b + Math.Sqrt(delta) / 2 / a) + "  x2: " + (-b - Math.Sqrt(delta) / 2 / a));
    }

}
else
{
    Console.WriteLine("Phương trình 1 nghiệm: "+ -c/b);
    
}

