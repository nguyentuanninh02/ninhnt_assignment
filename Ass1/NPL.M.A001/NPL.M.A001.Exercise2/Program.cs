﻿static long ConvertToBinary(long naturalNumber)
{
    long binaryNumber = 0;
    long p = 0;
    while (naturalNumber > 0)
    {
        binaryNumber += (naturalNumber % 2) * Convert.ToInt64(Math.Pow(10, p));
        p++;
        naturalNumber = naturalNumber / 2;
    }
    return binaryNumber;
}

Console.WriteLine("Input natural number: ");
int a = Convert.ToInt32(Console.ReadLine());
Console.WriteLine(ConvertToBinary(a));