﻿static int PrintFibonacci(int a)
{
    if (a <= 1) return 1;
    else return (PrintFibonacci(a - 1) + PrintFibonacci(a - 2));

}

Console.WriteLine("Input number: ");
int a= Convert.ToInt32(Console.ReadLine()); 
for(int i= 0; i<a; i++)
{
    Console.WriteLine(PrintFibonacci(i));
}