﻿static bool CheckPrimeNumber(int a)
{
    if(a < 2) return false;
    if ((a== 2))
    {
        return true;
    }
    for (int i = 2; i <= Math.Sqrt(a); i++)
    {
        if(a % i == 0) return false;
    }
    return true;
}
Console.WriteLine("Input number: ");
int a= Convert.ToInt32(Console.ReadLine());
Console.WriteLine(CheckPrimeNumber(a));