﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A008.Exercise
{
    internal class Student
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string Gender { get; set; }
        public string Relationship { get; set; }
        public DateTime EntryDate { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public decimal Mark { get; set; }
        public string Grade { get; set; }

        public Student() { }

        public Student(string name, string Class, string gender, DateTime entryDate, int age, string address, decimal mark = 0, string grade = "F", string relationship = "Single")
        {
            Name = name;
            this.Class = Class;
            Gender = gender;
            Relationship = relationship;
            EntryDate = entryDate;
            Age = age;
            Address = address;
            Mark = mark;
            Grade = grade;
        }

        public string? ToString(string Name, string Class, string Gender, string Relationship, int Age, string Grade)
        {
            return string.Format("{0, -15}{1, -15}{2, -15}{3, -20}{4, -15}{5, -15}", Name, Class, Gender, Relationship, Age, Grade);
        }

        public override string? ToString()
        {
            return base.ToString();
        }

        public string Graduate(decimal mark = 0)
        {
            if (mark >= 0M && mark <= 4.9M)
            {
                return "F";
            }
            if (mark >= 5.0M && mark <= 5.4M)
            {
                return "D";
            }
            if (mark >= 5.5M && mark <= 5.9M)
            {
                return "C";
            }
            if (mark >= 6.0M && mark <= 6.4M)
            {
                return "C+";
            }
            if (mark >= 6.5M && mark <= 6.9M)
            {
                return "B-";
            }
            if (mark >= 7.0M && mark <= 7.4M)
            {
                return "B";
            }
            if (mark >= 7.5M && mark <= 7.9M)
            {
                return "B+";
            }
            if (mark >= 8.0M && mark <= 8.4M)
            {
                return "A-";
            }
            if (mark >= 8.5M && mark <= 10.0M)
            {
                return "A";
            }
            return "F";
        }
    }

}
