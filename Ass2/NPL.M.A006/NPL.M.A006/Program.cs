﻿using NPL.M.A006;

int choice;

Manager manager = new Manager();
manager.InitData();
do
{
    Console.WriteLine("=========Assignment 06 - EmployeeManagement =========");
    Console.WriteLine("Please select the admin area you require");
    Console.WriteLine("1. Import Employee");
    Console.WriteLine("2. Display Employees Information.");
    Console.WriteLine("3. Search Employee");
    Console.WriteLine("4. Exit");
    Console.WriteLine("Enter Menu Option Number");
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            manager.ImportEmployee();
            break;
        case 2:
            manager.DisplayEmployee();
            break;
        case 3:
            manager.SearchEmployee();
            break;
        default:
            break;
    }
} while (choice> 0 && choice< 4);
