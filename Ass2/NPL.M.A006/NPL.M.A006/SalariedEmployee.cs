﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006
{
    internal class SalariedEmployee : Employee
    {
        public double comminssionRate { get; set; }
        public double grossSales { get; set; }
        public double basicSalary { get; set; }

        public SalariedEmployee() { }

        public SalariedEmployee(string ssn, string firstName, string lastName,
            DateTime birthDate, string phone, string email,
            double comminssionRate, double grossSales, double basicSalary) : base(ssn, firstName, lastName, birthDate, phone, email)
        {
            this.basicSalary = basicSalary;
            this.comminssionRate = comminssionRate;
            this.grossSales = grossSales;
        }

        public override string? ToString()
        {
            return string.Format("{0,-10}  {1,-15} {2,-15} {3,-15} {4,-10} {5,-20} {6,-5} {7,-5} {8,-5} ",
                ssn, firstName, lastName, birthDate.ToString("dd/MM/yyy"), phone, email, basicSalary, comminssionRate, grossSales);
        }
    }
}
