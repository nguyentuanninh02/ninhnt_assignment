﻿using System.Globalization;

string dateString = Console.ReadLine();
try
{
    DateTime date = DateTime.ParseExact(dateString, "dd/MM/yyyy", null);
    int count = 0;
    while (count< 5)
    {
        count++;
        if(count== 1)
        {
            for(int i= 0; i < 7; i++)
            {
                date= date.AddDays(1);
                if (date.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    date= date.AddDays(2);
                } else if (date.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    date= date.AddDays(1);
                }
            }
            Console.WriteLine("1st reminder: "+ date.ToString("dd/MM/yyyy"));
        }

        if(count== 2)
        {
            for (int i = 0; i < 2; i++)
            {
                date = date.AddDays(1);
                if (date.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    date = date.AddDays(2);
                }
                else if (date.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    date = date.AddDays(1);
                }
            }
            Console.WriteLine("2st reminder: " + date.ToString("dd/MM/yyyy"));
        }

        if (count>2)
        {
            for (int i = 0; i < 1; i++)
            {
                date = date.AddDays(1);
                if (date.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    date = date.AddDays(2);
                }
                else if (date.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    date = date.AddDays(1);
                }
            }
            Console.WriteLine(count+ "st reminder: " + date.ToString("dd/MM/yyyy"));
        }
    }
}
catch
{
    Console.WriteLine("Invalid format date");
}

